const url = require("url");
const qs = require("qs");

const User = require("./User");

const getAllOrders = (req, res) => {
  const urlObj = url.parse(req.url, true, false);
  const token = urlObj.query.token;
  console.log(`/getAllOrder requested`);
  if (token) {
    const order = User.printAllorders(token);
    if (order) {
      res.end(JSON.stringify(order));
      res.writeHeader(200);
      res.end();
    } else {
      invalidRequest(req, res);
    }
  } else {
    invalidRequest(req, res);
  }
};

const getOrder = (req, res) => {
  const urlObj = url.parse(req.url, true, false);
  const token = urlObj.query.token;
  const id = urlObj.query.id;

  console.log(`/getOrder?id${id} called! with ${token}`);

  if (!Number.isNaN(id)) {
    const order = User.getOrderbyId(token, id);

    if (order) {
      res.writeHeader(200);
      res.end(JSON.stringify(order));
    } else {
      invalidRequest(req, res);
    }
  } else {
    invalidRequest(req, res);
  }
};

const invalidRequest = (req, res) => {
  console.log(`invalid request`);
  res.statusCode = 404;
  res.setHeader("Content-Type", "text/plain");
  res.end("Invalid Request, please read API or enter valid values");
};

const addNewReservation = (req, res) => {
  console.log(`/addNewReservation requested`);
  let body = "";
  req.on("data", chunk => {
    body += chunk.toString();
  });
  req.on("end", () => {
    body = qs.parse(body);
    console.log(body);
    const reserve = User.addNewReservation(body.token, body.amount);
    if (reserve) {
      res.end(JSON.stringify(reserve));
      res.writeHeader(200);
      res.end();
    } else {
      invalidRequest(req, res);
    }
  });
};

const removeReservation = (req, res) => {
  console.log(`/removeReservation requested`);
  let body = "";
  req.on("data", chunk => {
    body += chunk.toString();
  });
  req.on("end", () => {
    body = qs.parse(body);
    const reserve = User.removeReservation(body.token, body.order_id);
    if (reserve) {
      res.end(reserve);
      res.writeHeader(200);
      res.end();
    } else {
      invalidRequest(req, res);
    }
  });
};

const removeAllReservations = (req, res) => {
  console.log(`/removeAllReservations requested`);
  let body = "";
  req.on("data", chunk => {
    body += chunk.toString();
  });
  req.on("end", () => {
    body = qs.parse(body);
    const reserve = User.removeAllReservations(body.token, body.order_id);
    if (!reserve) {
      invalidRequest(req, res);
    }
    res.end(reserve);
    res.writeHeader(200);
    res.end();
  });
};

const printLogs = (req, res) => {
  console.log(`/printLogs requested`);
  const urlObj = url.parse(req.url, true, false);
  const token = urlObj.query.token;

  if (!Number.isNaN(token)) {
    const logs = User.getLogs(token);
    if (logs) {
      res.end(logs);
      res.writeHeader(200);
      res.end();
    } else {
      invalidRequest(req, res);
    }
  } else {
    invalidRequest(req, res);
  }
};

const changeOrder = (req, res) => {
  const urlObj = url.parse(req.url, true, false);
  console.log(`/changeOrder requested`);
  const token = urlObj.query.token;
  const order_id = urlObj.query.order_id;
  const amount = urlObj.query.amount;
  const change = User.changeOrd(token, order_id, amount);
  if (!change) {
    invalidRequest(req, res);
  }
  res.end(change);
  res.writeHeader(200);
  res.end();
};

module.exports = {
  getAllOrders,
  invalidRequest,
  addNewReservation,
  getOrder,
  removeReservation,
  removeAllReservations,
  printLogs,
  changeOrder
};
