const http = require("http");
const url = require("url");

module.exports = http.createServer((req, res) => {
  var handler = require("./handler.js");
  const urlObj = url.parse(req.url, true, false);

  switch (req.method) {
    case "GET":
      if (urlObj.pathname === "/getAllOrders" && urlObj.query) {
        handler.getAllOrders(req, res);
      } else if (urlObj.pathname === "/getOrder" && urlObj.query) {
        const id = urlObj.query.id;
        res.writeHeader(200);
        handler.getOrder(req, res);
      } else if (urlObj.pathname === "/printLogs" && urlObj.query) {
        handler.printLogs(req, res);
      } else {
        handler.invalidRequest(req, res);
      }
      break;
    case "POST":
      if (req.url === "/addNewReservation" && urlObj.query) {
        handler.addNewReservation(req, res);
      } else if (req.url === "/removeReservation" && urlObj.query) {
        handler.removeReservation(req, res);
      } else if (req.url === "/removeAllReservations" && urlObj.query) {
        handler.removeAllReservations(req, res);
      } else {
        handler.invalidRequest(req, res);
      }
      break;
    case "PUT":
      if (urlObj.pathname === "/changeOrder" && urlObj.query) {
        handler.changeOrder(req, res);
      } else {
        handler.invalidRequest(req, res);
      }
      break;
  }
});
