const { EventEmitter } = require("events");
const fs = require("fs");
const moment = require("moment");
const jsonfile = require("jsonfile");
const os = require("os");

const users = require("./data/users.json");
const tickets = require("./data/tickets.json");

class User extends EventEmitter {
  constructor() {
    super();
    this._users = users;
    this._tickets = tickets;
  }

  getOrderbyId(token, id) {
    const mytickets = [];
    users.forEach(user => {
      if (token == user.token) {
        tickets.forEach(ticket => {
          if (ticket.order_id == id) {
            this.emit("getOrder", this._users[id - 1], id);
            mytickets.push(ticket);
          }
        });
      }
    });
    return mytickets;
  }

  getLogs(token) {
    let usr = users.find(user => token == user.token);
    if (!usr) {
      this.emit("error", usr);
      console.error("no user found");
      return;
    }
    if (usr.level == "admin") {
      this.emit("printLogs", usr);
      var data = fs.readFileSync("./server/data/log.txt", "utf8");
      return data.toString();
    } else {
      this.emit("error", usr);
      return console.error("user not admin");
    }
  }

  printAllorders(token) {
    let usr = users.find(user => token == user.token);
    if (!usr) {
      this.emit("error", usr);
      console.error("no user found");
      return;
    }
    if (usr.level == "admin") {
      this.emit("allOrders", usr);
      return tickets;
    }
  }

  addNewReservation(token, amount) {
    let usr = users.find(user => token == user.token);
    if (!usr) {
      this.emit("error");
      console.error("no user found");
      return;
    }



    let time = moment().format("LLLL");

    let fileString = fs.readFileSync("./server/data/tickets.json").toString();
    let fileObj = JSON.parse(fileString);

    let i;
    for (i = 1; i < 9999; i++) {
      //nice generation of order_id that not exist :D
      let ord = fileObj.find(fileObj => fileObj.order_id == i);
      if (!ord) {
        break;
      }
    }

    let amnt = parseInt(amount);
    let ticket = {
      order_id: i,
      user_id: usr.id,
      amount: amnt,
      order_stamp: time
    };

    let total = 0;
    fileObj.forEach(ticket => {
      total = ticket.amount + total;
    });

    if (total + parseInt(amount) > 10) {
      this.emit("error");
      console.error("max orders num exided");
      return;
    }

    if (parseInt(amount) <= 0) {
      this.emit("error");
      console.error("0 tickets order");
      return;
    }

    fileObj.push(ticket);
    jsonfile.writeFileSync("./server/data/tickets.json", fileObj, {
      spaces: 2,
      EOL: "\r\n"
    });

    this.emit("addReservation", usr);

    return ticket;
  }

  removeReservation(token, id) {
    let usr = users.find(user => user.token == token);
    if (!usr) {
      this.emit("error");
      console.error("no user found");
      return;
    }
    let ord = tickets.find(tickets => tickets.order_id == id);
    if (!ord) {
      this.emit("error");
      console.error("no order found");
      return;
    }

    let fileString = fs.readFileSync("./server/data/tickets.json").toString();
    let fileObj = JSON.parse(fileString);

    fileObj.pop(ord);

    jsonfile.writeFileSync("./server/data/tickets.json", fileObj, {
      spaces: 2,
      EOL: "\r\n"
    });
    this.emit("removeReservation", usr);
    return "Order deleted";
  }

  removeAllReservations(token) {
    let usr = users.find(user => user.token == token);
    if (!usr) {
      this.emit("error");
      console.error("no user found");
      return;
    }
    if (usr.level == "admin") {
      jsonfile.writeFileSync("./server/data/tickets.json", []);
      this.emit("removeAllOrders", usr);
    } else {
      this.emit("error", usr);
      console.error("no admin rights");
      return;
    }
    return "done";
  }

  changeOrd(token, ord_id, amnt) {
    let usr = users.find(user => token == user.token);
    if (!usr) {
      this.emit("error");
      console.error("no user found");
      return;
    }
    let ord = tickets.find(tickets => tickets.order_id == ord_id);
    if (!ord) {
      this.emit("error");
      console.error("no order found");
      return;
    }

    let ticket = {
      order_id: ord.order_id,
      user_id: usr.id,
      amount: amnt,
      order_stamp: ord.order_stamp
    };

    let total = 0;
    tickets.forEach(ticket => {
      total = ticket.amount + total;
    });
    total = total - ord.amount;

    if (total + parseInt(amnt) > 10) {
      this.emit("error");
      console.error("max orders num exided");
      return;
    }

    let fileString = fs.readFileSync("./server/data/tickets.json").toString();
    let fileObj = JSON.parse(fileString);

    fileObj.pop(ord);
    fileObj.push(ticket);

    jsonfile.writeFileSync("./server/data/tickets.json", fileObj, {
      spaces: 2,
      EOL: "\r\n"
    });

    this.emit("changeOrder", usr);

    return "order changed";
  }
}

const userRepo = new User();

userRepo.on("getOrder", (users, id) => {
  let message = `user id:${users.id} is requested ticket ${id}` + os.EOL;
  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

userRepo.on("allOrders", usr => {
  let message = `user id:${usr.id} is requested all tickets` + os.EOL;
  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

userRepo.on("printLogs", usr => {
  let message = `user id:${usr.id} is requested all logs` + os.EOL;
  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

userRepo.on("removeAllOrders", usr => {
  let message = `user id:${usr.id} is requested to delete all orders` + os.EOL;
  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

userRepo.on("addReservation", usr => {
  let message = `user id:${usr.id} is added reservation` + os.EOL;
  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

userRepo.on("removeReservation", usr => {
  let message = `user id:${usr.id} is removed reservation` + os.EOL;
  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

userRepo.on("changeOrder", usr => {
  let message = `user id:${usr.id} changed reservation` + os.EOL;
  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

userRepo.on("error", usr => {
  let message;
  if (usr) {
    message = `user id:${usr.id} requested invalid request` + os.EOL;
  } else {
    message = "unknown user requested invalid request" + os.EOL;
  }

  console.log(message);
  fs.appendFileSync("./server/data/log.txt", message);
});

module.exports = userRepo;
