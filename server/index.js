const port = process.env.PORT || 8080;

const server = require('./controller.js');

server.listen(port, () => {
	console.log(`Server running on port ${port}`);
});
	